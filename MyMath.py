import math

class MyMath:

    def __init__(self, radius):
        self.radius = radius

    def getCirclePerimeter(self):
        return round(2 * math.pi * self.radius, 2)
    
    def getCircleArea(self):
        return round(math.pi * self.radius * self.radius, 2)

    def getSphereSurfaceArea(self):
        return round(4 * self.getCircleArea(), 2)
    
    def getSphereVolumn(self):
        return round(0.75 * self.getCircleArea() * self.radius, 2)

test = MyMath(5)
print(test.getCirclePerimeter())
print(test.getCircleArea())
print(test.getSphereSurfaceArea())
print(test.getSphereVolumn())