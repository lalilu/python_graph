
class Temperature:

    def __init__(self, degree):
        self.degree = degree

    def ToFahrenheit(self):
        return self.degree * 1.8 + 32

    def ToCelsius(self):
        return (self.degree - 32) * 5 / 9

c = Temperature(30)
print(c.ToFahrenheit())

f = Temperature(86)
print(f.ToCelsius())